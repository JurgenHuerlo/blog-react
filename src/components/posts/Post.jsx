import React from 'react';
import {Link} from 'react-router-dom';

export default function Post({post}) {
    return (
        <div className="col s4">
            <div className="card z-depth-4">
                <div className="card-image waves-effect waves-block waves-light">
                    <img className="activator" src={post.image} alt="post"></img>
                </div>
                <div className="card-content">
                    <span className="activator grey-text text-darken-4 truncate">{post.title}</span>
                    <p className="light hide-on-small-only"><span><i className="material-icons">date_range</i></span>{"\n"+ post.created.slice(0,-22)}</p>
                </div>
                <div className="card-reveal">
                    <i className="card-title material-icons right">close</i>
                    <span className="card-title grey-text text-darken-4 hide-on-small-only">{post.title}</span>
                    <p className="hide-on-small-only">{post.description}</p><br/><br/>
                    <Link className="right red waves-effect waves-light btn-small" to={`/post/detail/${post.id}`}>Ver más</Link>
                </div>
            </div>
        </div>
    )
}
