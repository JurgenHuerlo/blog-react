import React,{Fragment} from 'react';
import Post from './Post'

export default function Posts({posts}) {
    return (
        <Fragment>
            <div className="container">
                <h4 className="center"><em>Publicaciones.</em></h4>
                <hr className="z-depth-2" /><br />
            </div>
            <div className="row">
                {posts.map(post => (
                    <Post key={post.id} post={post}/>
                ))}
            </div>
        </Fragment>
    )
}
