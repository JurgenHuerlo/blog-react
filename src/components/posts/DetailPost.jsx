import React from 'react';
import {Link} from 'react-router-dom';

export default function DetailPost({content}) {
    var post = content.content;
    return (
        <div className="container">
            <div className="card-panel z-depth-4">
                <Link to="/posts"><i className="left material-icons">arrow_back</i></Link>
                <h3 className="center"><i>{content.title}</i></h3>
                <div className="container center">
                    <img src={content.image} className="responsive-img" alt="post"/>
                </div>
                <br/>
                <p dangerouslySetInnerHTML={{__html:post}}></p>

            </div>
        </div>
    )
}
