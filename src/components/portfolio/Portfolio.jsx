import React from 'react';
import Slider from 'react-animated-slider';
import horizontalCss from 'react-animated-slider/build/horizontal.css';
import './styles.css';

export default function Portfolio({proyects}) {
    return (
        <div>
            <div className="section">
                <div className="container">
                    <h3 className="center"><em>Mis proyectos</em></h3>
                    <hr className="z-depth-2" /><br/><br/>
                </div>
                <Slider classNames={horizontalCss} autoplay={3000}>
                    {proyects.map((item, index) => (
                        <div
                            key={index}
                            style={{ background: `url('${item.image}') no-repeat center center` }}
                        >
                            <div className="center"><br/>
                                <h4 className="hide-on-small-only cyan-text"><i>{item.title}</i></h4>
                            </div>
                        </div>
                    ))}
                </Slider>
                <br/>
                <p className="light center"><strong><em> Si te gusta uno de estos proyectos mandame un correo en la sección de contacto</em></strong></p>
            </div>
        </div>
    )
}