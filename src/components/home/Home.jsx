import React,{Fragment} from 'react';
import programing from './img1.jpeg';
import './styles.css';

export default function Home() {
    return (
        <Fragment>
            <div className="grey lighten-2">
                <img src={programing} className="img-complete" alt="Computer" />
                <div className="container">
                    <div className="center">
                        <h3><em>Bienvenido al Noob blog</em></h3>
                        <hr className="z-depth-2" />
                        <br />
                    </div>
                    <div className="container">
                        <div className="section">
                            <div className="row">
                                <div className="col s12 m4">
                                    <div className="icon-block">
                                        <h2 className="center light-blue-text"><i className="large material-icons">info_outline</i></h2>
                                        <h5 className="center">¿Qué hay?</h5>
                                        <h6 className="light">En este sitio web podrás encontrar informacion que pueda serte de mucha ayuda, como tutoriales, recomendaciones entre otras cosas más que
                                    pueden ser de tu agrado.</h6>
                                    </div>
                                </div>
                                <div className="col s12 m4">
                                    <div className="icon-block">
                                        <h2 className="center light-blue-text"><i className="large material-icons">person_outline</i></h2>
                                        <h5 className="center">¿Quién soy?</h5>
                                        <h6 className="light">Actualmente estudio la carrera de ingenieria en Sistemas en la Universidad
                                        Catolica del Ecuador sede Esmeraldas, me gusta bastante la programación web tanto en frontend como en backend.</h6>
                                    </div>
                                </div>
                                <div className="col s12 m4">
                                    <div className="icon-block">
                                        <h2 className="center light-blue-text"><i className="large material-icons">business_center</i></h2>
                                        <h5 className="center">Mis proyectos</h5>
                                        <h6 className="light">Uno de los objetivos de esta pagina es de mostrar mis proyectos los cuales he realizado a lo largo de mis estudios,
                                        si te interesa alguno de ellos puedes contactarte conmigo y conversar si es que lo quieres implementar como un sitio web</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container section">
                            <h5 className="light"><em>¿Qué esperas? dale al botón de menu y empieza a navegar por posts que te pueden interesar.</em></h5>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
