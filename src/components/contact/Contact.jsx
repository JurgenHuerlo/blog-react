import React from 'react'

export default function Contact() {
    return (
        <div className="container">
            <div className="row">
                <form className="col s12"><br/><br/>
                    <div className="row card-panel z-depth-4">
                        <h2 className="center light"><i> Contáctame</i></h2><br/>

                        <div className="input-field col s6">
                            <i className="material-icons prefix">account_circle</i>
                            <input type="text" className="validate" id="nombres" required />
                            <label htmlFor="nombres">Nombres</label>
                        </div>

                        <div className="input-field col s6">
                            <i className="material-icons prefix">email</i>
                            <input type="email" className="validate" id="email" required />
                            <label htmlFor="email">Email</label>
                        </div>

                        <div className="input-field col s12">
                            <i className="material-icons prefix">question_answer</i>
                            <input type="text" className="validate" id="asunto" required />
                            <label htmlFor="asunto">Asunto</label>
                        </div>

                        <div className="input-field col s12">
                            <i className="material-icons prefix">message</i>
                            <textarea id="message" className="materialize-textarea validate" required ></textarea>
                            <label htmlFor="message">Mensaje</label>
                        </div>

                        <div className="center">
                            <input type="submit" className="waves-effect waves-light btn-large red" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
