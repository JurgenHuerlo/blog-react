import React from 'react';
import img from '../sidebar/logo.jpeg'

export default function About() {
    return (
        <div className="container">
            <div className="card-panel z-depth-4">
                <h1 className="center light"><i>Acerca del autor</i></h1>
                <div className="center">
                    <img src={img} alt="author" className="responsive-img circle"/>
                </div>
                <div className="container">
                    <h6>Soy un estudiante de la Pontificia Universidad Católica del Ecuador sede Esmeraldas (PUCESE)
                        de la carrera de Ingeniería en Sistemas, me gusta mucho programar, jugar video juegos, salir y pasar
                        tiempo con mis amigos, tocar la guitarra y viajar estos son mis pasatiempos favoritos cuando no estoy estudiando. 
                        Actualmente vivo en la ciudad de Esmeraldas mientras termino mis estudios, siempre trato de estar al día con las nuevas tecnologías, 
                        aunque estas avancen muy rápido, espero que el blog sea de su agrado ya que en este también encontrarán muchos de mis proyectos personales.
                    </h6>
                    <h6><i>¿Qué herramientas uso para desarrollar?</i></h6>
                    <h6>Actualmente para el backend dependiendo del proyecto suelo utilizar herramientas como Django y su DRF, 
                        Firebase y Ocasionalmente suelo utilizar Express.js, Postgresql lo utilizo con el ORM de Django, MongoDB también dependiendo del
                        proyecto.
                        <br/> Para el frontend utilizo la libreria React.js para crear aplicaciones muy rápidas bajo el concepto de las SPA, dependiendo 
                        del proyecto también suelo utilizar html, css y js  con librerías como bootstrap o materialize, que ayudan al diseño de la página y hacerla 
                        responsive.
                    </h6>
                </div>
            </div>
        </div>
    )
}
