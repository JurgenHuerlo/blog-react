import React,{Fragment} from 'react'
import {NavLink} from 'react-router-dom';
import Logo from './logo.jpeg';
import Office from './office.jpg';


export default function SideBar() {

    return (
        <Fragment>
            <div className="fixed-action-btn">
                <button className="btn-floating btn-large red sidenav-trigger z-depth-4 waves-effect" data-target="slide-out">
                    <i className="large material-icons">menu</i>
                </button>
            </div>
            <ul id="slide-out" className="grey lighten-2 sidenav">
                <li><div className="user-view">
                        <div className="background">
                            <img src={Office} alt="Office background"></img>
                        </div>
                        <NavLink to="/about"><img className="circle" src={Logo} alt="Page owner icon"></img></NavLink>
                        <p><span className="white-text name">Jurgen Huerlo</span></p>
                        <NavLink to="/contact"><span className="white-text email">jurgenhuerlo25@gmail.com</span></NavLink>
                </div></li>
                    <li><NavLink to="/" className="waves-effect waves-red"><i className="material-icons">home</i>Inicio</NavLink></li>
                    <li><NavLink to="/posts" className="waves-effect waves-red"><i className="material-icons">art_track</i>Posts</NavLink></li>
                    <li><NavLink to="/portfolio" className="waves-effect waves-red"><i className="material-icons">business_center</i>Mis Proyectos</NavLink></li>
                    <li><NavLink to="/about" className="waves-effect waves-red"><i className="material-icons">person</i>Acerca de mí</NavLink></li>
                    <li><NavLink to="/contact" className="waves-effect waves-red"><i className="material-icons">contact_mail</i>Contactame</NavLink></li>
                    <li className="black divider"></li>
                    <li><p className="subheader center">Copyright © 2019 webtechq.com</p></li>
            </ul>
        </Fragment>
    )   
}
