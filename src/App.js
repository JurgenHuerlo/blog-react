import React,{useState,useEffect} from 'react';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import axios from 'axios';

//Componentes
import SideBar from './components/sidebar/SideBar';
import Posts from './components/posts/Posts';
import Contact from './components/contact/Contact';
import About from './components/about/About';
import Home from './components/home/Home';
import Portfolio from './components/portfolio/Portfolio';
import DetailPost from './components/posts/DetailPost';

function App() {

  const [posts, SetPosts] = useState([]);
  const [proyects, SetProyects] = useState([]);

  useEffect(()=>{
    const consultarPosts = async () =>{
      //Consultando la Api
      const resultado = await axios.get('https://blogapi.pythonanywhere.com/api/v1/post/');
      SetPosts(resultado.data);
    }
    consultarPosts();
    const consultarProyects = async () =>{
      //Consultando la Api
      const resultado = await axios.get('https://blogapi.pythonanywhere.com/api/v1/proyects/');
      SetProyects(resultado.data);
    }
    consultarProyects();
  },[])

  
  return (
    <Router>
      <SideBar/>
      <Switch>
      <Route exact path="/" component={Home}/>
      <Route exact path="/posts" render={()=>(
        <Posts posts={posts}/>
      )} />
      <Route exact path="/portfolio" render={()=>(
        <Portfolio proyects={proyects}/>
      )} />
      <Route exact path="/about" component={About} />
      <Route exact path="/contact" component={Contact}/>
      <Route exact path="/post/detail/:id" render={props=>{
        //Tomando el id del lab
        const idPost = parseInt(props.match.params.id);
        //lab que se pasa al state 
        const post = posts.filter(pub => pub.id === idPost);
        return (
          <DetailPost content={post[0]}/>
            )
      }} />

      </Switch>
    </Router>
  );
}

export default App;
